import React from 'react';
import {ImageBackground, ToastAndroid, Alert} from 'react-native';
import {MainComponentStyle} from './main.style';
import {connect} from 'react-redux';
import {
  Container,
  Content,
  Button,
  Icon,
  Text,
  View,
  Form,
  Item,
  Input,
  Picker,
  Label,
  Switch,
  Tab,
  Tabs,
  ScrollableTab,
  TabHeading,
} from 'native-base';
import {Col, Row, Grid} from 'react-native-easy-grid';
import NumericInput from 'react-native-numeric-input';
import DatePicker from 'react-native-modal-datetime-picker';
import ValidatorService from '../Shared/Services/validatorService';
import {DELIVERY_ENUM} from '../Shared/Utils/InputEnum';

const styleComponent = MainComponentStyle.getStyle();

const bg = require('../images/justeat.jpg');
const supportedList = [
  'as soon as possible',
  'this morning',
  'this afternoon',
  'this evening',
  'tomorrow morning',
  'tomorrow afternoon',
  'tomorrow evening',
  'in two days, morning',
  'in two days, afternoon',
  'in two days, evening',
];
const typeList = ['type 1', 'type 2', 'type 3'];
class MainComponent extends React.Component {
  delivery: DeliveryInterface;
  validatorService = new ValidatorService();

  constructor(props) {
    super(props);
    this.delivery = this.props.delivery;
    this.state = {
      load: false,
      deposit_availability_start: false,
      deposit_availability_end: false,
      refresh: false,
      express: this.delivery.express,
    };
  }

  refresh() {
    this.setState({
      refresh: !this.state.refresh,
    });
  }

  componentDidMount() {
    this.addOptions();
  }

  onValueChange(value: string) {
    this.delivery.supported = value;
  }

  handleDeposit_availability_start = date => {
    this.delivery.deposit_availability_start = date;
    this.toggleDeposit_availability_start();
  };

  handleDeposit_availability_end = date => {
    this.delivery.deposit_availability_end = date;
    this.toggleDeposit_availability_end();
  };

  toggleDeposit_availability_start = () => {
    this.setState({
      deposit_availability_start: !this.state.deposit_availability_start,
    });
  };

  toggleDeposit_availability_end = () => {
    this.setState({
      deposit_availability_end: !this.state.deposit_availability_end,
    });
  };
  tooggleSwitch(value) {
    this.delivery.switch = value;
    this.setState({
      express: value,
    });
  }

  setQuantity(value) {
    this.delivery.quantity = value;
    this.addOptions();
  }

  addOptions() {
    this.delivery.options = [];
    for (let index = 0; index < this.delivery.quantity; index++) {
      this.delivery.options.push({
        type: null,
        l: null,
        ll: null,
        h: null,
        weight: null,
        comments: null,
      });
    }
    this.refresh();
  }

  confirm() {
    if (this.validatorService.validatorDeliveryForm(this.delivery)) {
      const action = {type: DELIVERY_ENUM.SET_INFOS, value: this.delivery};
      this.props.dispatch(action);
      alert(JSON.stringify(this.delivery));
    } else {
      ToastAndroid.show(
        'please fill in the form correctly',
        ToastAndroid.SHORT,
      );
    }
  }

  render() {
    return (
      <Container>
        <Content>
          <View>
            <View style={styleComponent.container}>
              <ImageBackground source={bg} style={styleComponent.imageBg}>
                <Form style={styleComponent.form}>
                  <View>
                    <Item style={styleComponent.itemInput}>
                      <Input
                        placeholderTextColor={
                          styleComponent.placeholderTextColor.color
                        }
                        style={styleComponent.input}
                        value={this.delivery.support_address}
                        onChangeText={text => {
                          this.delivery.support_address = text;
                          this.refresh();
                        }}
                        placeholder="support address"
                      />
                    </Item>
                  </View>
                  <View>
                    <Item style={styleComponent.itemInput}>
                      <Input
                        placeholderTextColor={
                          styleComponent.placeholderTextColor.color
                        }
                        style={styleComponent.input}
                        value={this.delivery.deposit_address}
                        onChangeText={text => {
                          this.delivery.deposit_address = text;
                          this.refresh();
                        }}
                        placeholder="deposit address"
                      />
                    </Item>
                  </View>
                  <View>
                    <Grid>
                      <Col size={25}>
                        <Label style={styleComponent.label}>supported</Label>
                      </Col>
                      <Col size={75}>
                        <Item style={styleComponent.itemInput}>
                          <Picker
                            note
                            mode="dropdown"
                            selectedValue={this.delivery.supported}
                            onValueChange={text => {
                              this.delivery.supported = text;
                              this.refresh();
                            }}
                            placeholderTextColor={
                              styleComponent.placeholderTextColor.color
                            }
                            placeholder="As Soon as possible">
                            <Picker.Item
                              label="Select an element"
                              value={null}
                            />
                            {supportedList.map((supported, i) => {
                              return (
                                <Picker.Item
                                  key={i}
                                  label={supported}
                                  value={supported}
                                />
                              );
                            })}
                          </Picker>
                        </Item>
                      </Col>
                    </Grid>
                  </View>
                  <View style={styleComponent.marginTop}>
                    <Label style={styleComponent.label}>recipient’s name</Label>
                    <Item style={styleComponent.itemInput}>
                      <Input
                        placeholderTextColor={
                          styleComponent.placeholderTextColor.color
                        }
                        style={styleComponent.input}
                        value={this.delivery.recipient}
                        onChangeText={text => {
                          this.delivery.recipient = text;
                          this.refresh();
                        }}
                        placeholder="recipient’s name"
                      />
                    </Item>
                  </View>
                  <View style={styleComponent.flexRow}>
                    <View style={styleComponent.viewLabel}>
                      <Label style={styleComponent.label}>express</Label>
                    </View>
                    <View style={styleComponent.viewPicker}>
                      <Switch
                        trackColor={{true: '#DDDDDD', false: '#FFFFFF'}}
                        style={styleComponent.switch}
                        onValueChange={value => this.tooggleSwitch(value)}
                        value={this.state.express}
                      />
                    </View>
                  </View>
                  <View style={styleComponent.marginTop}>
                    <Grid>
                      <Col size={55}>
                        <Text style={styleComponent.label}>
                          deposit availabilities :
                        </Text>
                      </Col>
                      <Col size={15}>
                        <Button
                          transparent
                          onPress={this.toggleDeposit_availability_start}>
                          <Icon
                            active
                            name="time"
                            style={styleComponent.colorWhite}
                          />
                        </Button>
                        <DatePicker
                          isVisible={this.state.deposit_availability_start}
                          onConfirm={this.handleDeposit_availability_start}
                          onCancel={this.toggleDeposit_availability_start}
                          mode="time"
                          locale="fr_FR"
                          date={new Date()}
                        />
                      </Col>
                      <Col size={15}>
                        <Text style={styleComponent.label}> to</Text>
                      </Col>
                      <Col size={15}>
                        <Button
                          transparent
                          onPress={this.toggleDeposit_availability_end}>
                          <Icon
                            active
                            name="time"
                            style={styleComponent.colorWhite}
                          />
                        </Button>
                        <DatePicker
                          isVisible={this.state.deposit_availability_end}
                          onConfirm={this.handleDeposit_availability_end}
                          onCancel={this.toggleDeposit_availability_end}
                          mode="time"
                          locale="fr_FR"
                          date={new Date()}
                        />
                      </Col>
                    </Grid>
                  </View>
                </Form>
              </ImageBackground>
            </View>
            <View style={styleComponent.bg}>
              <Form style={styleComponent.form}>
                <View style={styleComponent.flexRow}>
                  <View style={styleComponent.viewLabel}>
                    <Label style={styleComponent.black}>quantity</Label>
                  </View>
                  <View style={styleComponent.viewPicker}>
                    <NumericInput
                      minValue={1}
                      value={this.delivery.quantity}
                      onChange={value => this.setQuantity(value)}
                    />
                  </View>
                </View>
                <View style={styleComponent.marginTop10}>
                  {this.delivery.options.length > 0 && (
                    <View>
                      <Tabs
                        tabBarUnderlineStyle={
                          styleComponent.tabBarUnderlineStyle
                        }
                        renderTabBar={() => (
                          <ScrollableTab style={styleComponent.bg} />
                        )}>
                        {this.delivery.options.map((options, i) => {
                          return (
                            <Tab
                              key={i}
                              heading={
                                <TabHeading style={styleComponent.bg}>
                                  <Text style={styleComponent.color2}>
                                    Object #{i + 1}
                                  </Text>
                                </TabHeading>
                              }>
                              <View style={styleComponent.viewOptions}>
                                <View>
                                  <View>
                                    <Label style={styleComponent.labelOptions}>
                                      Type
                                    </Label>
                                  </View>
                                  <Item style={styleComponent.itemInput}>
                                    <Picker
                                      note
                                      mode="dropdown"
                                      selectedValue={options.type}
                                      onValueChange={text => {
                                        options.type = text;
                                        this.refresh();
                                      }}
                                      placeholderTextColor={
                                        styleComponent.placeholderTextColor
                                          .color
                                      }
                                      placeholder="As Soon as possible">
                                      <Picker.Item
                                        label="Select an element"
                                        value={null}
                                      />
                                      {typeList.map((type, i) => {
                                        return (
                                          <Picker.Item
                                            key={i}
                                            label={type}
                                            value={type}
                                          />
                                        );
                                      })}
                                    </Picker>
                                  </Item>
                                </View>
                                <View>
                                  <Grid>
                                    <Col>
                                      <View>
                                        <Grid>
                                          <Col size={25}>
                                            <Label
                                              style={
                                                styleComponent.optionLabel
                                              }>
                                              L :
                                            </Label>
                                          </Col>
                                          <Col size={75}>
                                            <Item
                                              style={styleComponent.itemInput}>
                                              <Input
                                                placeholderTextColor={
                                                  styleComponent
                                                    .optionplaceholderTextColor
                                                    .color
                                                }
                                                style={
                                                  styleComponent.optionInput
                                                }
                                                value={options.l}
                                                onChangeText={text => {
                                                  options.l = text;
                                                  this.refresh();
                                                }}
                                                placeholder="cm"
                                                keyboardType="numeric"
                                              />
                                            </Item>
                                          </Col>
                                        </Grid>
                                      </View>
                                    </Col>
                                    <Col>
                                      <View>
                                        <Grid>
                                          <Col size={25}>
                                            <Label
                                              style={
                                                styleComponent.optionLabel
                                              }>
                                              H :
                                            </Label>
                                          </Col>
                                          <Col size={75}>
                                            <Item
                                              style={styleComponent.itemInput}>
                                              <Input
                                                placeholderTextColor={
                                                  styleComponent
                                                    .optionplaceholderTextColor
                                                    .color
                                                }
                                                style={
                                                  styleComponent.optionInput
                                                }
                                                value={options.h}
                                                onChangeText={text => {
                                                  options.h = text;
                                                  this.refresh();
                                                }}
                                                placeholder="cm"
                                                keyboardType="numeric"
                                              />
                                            </Item>
                                          </Col>
                                        </Grid>
                                      </View>
                                    </Col>
                                  </Grid>
                                </View>
                                <View>
                                  <Grid>
                                    <Col>
                                      <View>
                                        <Grid>
                                          <Col size={25}>
                                            <Label
                                              style={
                                                styleComponent.optionLabel
                                              }>
                                              I :
                                            </Label>
                                          </Col>
                                          <Col size={75}>
                                            <Item
                                              style={styleComponent.itemInput}>
                                              <Input
                                                placeholderTextColor={
                                                  styleComponent
                                                    .optionplaceholderTextColor
                                                    .color
                                                }
                                                style={
                                                  styleComponent.optionInput
                                                }
                                                value={options.ll}
                                                onChangeText={text => {
                                                  options.ll = text;
                                                  this.refresh();
                                                }}
                                                placeholder="cm"
                                                keyboardType="numeric"
                                              />
                                            </Item>
                                          </Col>
                                        </Grid>
                                      </View>
                                    </Col>
                                    <Col>
                                      <View>
                                        <Grid>
                                          <Col size={25}>
                                            <Label
                                              style={
                                                styleComponent.optionLabel
                                              }>
                                              W :
                                            </Label>
                                          </Col>
                                          <Col size={75}>
                                            <Item
                                              style={styleComponent.itemInput}>
                                              <Input
                                                placeholderTextColor={
                                                  styleComponent
                                                    .optionplaceholderTextColor
                                                    .color
                                                }
                                                style={
                                                  styleComponent.optionInput
                                                }
                                                value={options.weight}
                                                onChangeText={text => {
                                                  options.weight = text;
                                                  this.refresh();
                                                }}
                                                placeholder="cm"
                                                keyboardType="numeric"
                                              />
                                            </Item>
                                          </Col>
                                        </Grid>
                                      </View>
                                    </Col>
                                  </Grid>
                                </View>
                                <View>
                                  <View>
                                    <Label style={styleComponent.labelOptions}>
                                      Comments
                                    </Label>
                                  </View>
                                  <Item style={styleComponent.optionitemInput}>
                                    <Input
                                      placeholderTextColor={
                                        styleComponent
                                          .optionplaceholderTextColor.color
                                      }
                                      style={styleComponent.input}
                                      value={options.comments}
                                      onChangeText={text => {
                                        options.comments = text;
                                        this.refresh();
                                      }}
                                      placeholder="Comments"
                                    />
                                  </Item>
                                </View>
                              </View>
                            </Tab>
                          );
                        })}
                      </Tabs>
                    </View>
                  )}
                  <View style={styleComponent.confirm}>
                    <Button
                      onPress={() => this.confirm()}
                      rounded
                      style={styleComponent.confirmBtn}>
                      <Text style={styleComponent.center}>Confirm</Text>
                    </Button>
                  </View>
                </View>
              </Form>
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {delivery: state.delivery};
};
export default connect(mapStateToProps)(MainComponent);
