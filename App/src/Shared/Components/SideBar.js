import React from 'react';
import {Container, Content, Text} from 'native-base';
import {connect} from 'react-redux';

import appStyle from '../Styles/app';
const styleComponent = appStyle.getStyle();

class SideBarComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      modal: false,
    };
  }

  render() {
    return (
      <Container>
        <Content>
          <Text>This is Content Section</Text>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {};
};
export default connect(mapStateToProps)(SideBarComponent);
