import React from 'react';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createDrawerNavigator} from 'react-navigation-drawer';
import {createStackNavigator} from 'react-navigation-stack';

import MainComponent from './Components/main.component';
import SideBarComponent from './Shared/Components/SideBar';

const Main = createDrawerNavigator(
  {
    home: {
      screen: MainComponent,
    },
  },
  {
    navigationOptions: {},
    contentComponent: props => <SideBarComponent {...props} />,
  },
);

const App = createStackNavigator(
  {
    main: Main,
  },
  {
    headerMode: 'none',
  },
);

const navigation = createSwitchNavigator(
  {
    app: App,
  },
  {
    initialRouteName: 'app',
  },
);
const AppContainer = createAppContainer(navigation);
export default AppContainer;
